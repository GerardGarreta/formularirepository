package com.example.preferencesproject;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    EditText etName, etSurname, etWeb, etTelf;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etName = findViewById(R.id.getName);
        etSurname = findViewById(R.id.getSurname);
        etWeb = findViewById(R.id.getWeb);
        etTelf = findViewById(R.id.getTelf);
    }

    public void buttonGoPressed(View view) {
        String valorName = etName.getText().toString();
        String valorSurname = etSurname.getText().toString();
        String valorWeb = etWeb.getText().toString();
        String valorTelf = etTelf.getText().toString();

        Intent intent = new Intent(MainActivity.this, SecondActivity.class);
        intent.putExtra("valor_name", valorName);
        intent.putExtra("valor_surname", valorSurname);
        intent.putExtra("valor_web", valorWeb);
        intent.putExtra("valor_telf",valorTelf);
        startActivity(intent);
    }

    private void deleteContent() {
        etName.setText("");
        etSurname.setText("");
        etWeb.setText("");
        etTelf.setText("");
    }

    public void Clear(View view)
    {

        openDialog();
    }

    public void openDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);
        alertDialogBuilder.setTitle(getString(R.string.app_name));
        alertDialogBuilder.setMessage( getString(R.string.questionDelete))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.btn_yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        deleteContent();
                    }
                })
                .setNegativeButton(getString(R.string.btn_no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}
