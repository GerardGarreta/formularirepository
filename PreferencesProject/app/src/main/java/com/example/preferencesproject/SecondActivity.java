package com.example.preferencesproject;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {
    TextView textName, textSurname, textWeb, textTelf,textCount;
    String valor_name, valor_surname, valor_web, valor_telf;
    int count;
    SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        prefs = getSharedPreferences("MisPreferencias", Context.MODE_PRIVATE);
        incrementCount();
        inicializateText();
        inicializateStrings();
        showResult();
    }

    private void incrementCount(){
        count=prefs.getInt("count",0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt("count", count+1);
        editor.commit();
    }

    private void inicializateText(){
        textName = findViewById(R.id.txtResultName);
        textSurname = findViewById(R.id.txtResultSurname);
        textWeb = findViewById(R.id.txtResultWeb);
        textTelf = findViewById(R.id.txtResultTelf);
        textCount = findViewById(R.id.txtResultCount);
    }

    private void inicializateStrings(){
        valor_name = getIntent().getStringExtra("valor_name");
        valor_surname = getIntent().getStringExtra("valor_surname");
        valor_web = getIntent().getStringExtra("valor_web");
        valor_telf = getIntent().getStringExtra("valor_telf");
    }

    private void showResult(){
        textName.setText(valor_name);
        textSurname.setText(valor_surname);
        textWeb.setText(valor_web);
        textTelf.setText(valor_telf);
        textCount.setText(""+count);
    }

    public void showWeb(View view){
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("http://"+valor_web));
        startActivity(intent);
    }

    public void llamada(View view){
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:"+valor_telf));
        startActivity(intent);
    }

}
